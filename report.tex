\documentclass[english]{article}\usepackage[]{graphicx}\usepackage[]{color}
%% maxwidth is the original width if it is less than linewidth
%% otherwise use linewidth (to make sure the graphics do not exceed the margin)
\makeatletter
\def\maxwidth{ %
  \ifdim\Gin@nat@width>\linewidth
    \linewidth
  \else
    \Gin@nat@width
  \fi
}
\makeatother

\definecolor{fgcolor}{rgb}{0.345, 0.345, 0.345}
\newcommand{\hlnum}[1]{\textcolor[rgb]{0.686,0.059,0.569}{#1}}%
\newcommand{\hlstr}[1]{\textcolor[rgb]{0.192,0.494,0.8}{#1}}%
\newcommand{\hlcom}[1]{\textcolor[rgb]{0.678,0.584,0.686}{\textit{#1}}}%
\newcommand{\hlopt}[1]{\textcolor[rgb]{0,0,0}{#1}}%
\newcommand{\hlstd}[1]{\textcolor[rgb]{0.345,0.345,0.345}{#1}}%
\newcommand{\hlkwa}[1]{\textcolor[rgb]{0.161,0.373,0.58}{\textbf{#1}}}%
\newcommand{\hlkwb}[1]{\textcolor[rgb]{0.69,0.353,0.396}{#1}}%
\newcommand{\hlkwc}[1]{\textcolor[rgb]{0.333,0.667,0.333}{#1}}%
\newcommand{\hlkwd}[1]{\textcolor[rgb]{0.737,0.353,0.396}{\textbf{#1}}}%

\usepackage{framed}
\makeatletter
\newenvironment{kframe}{%
 \def\at@end@of@kframe{}%
 \ifinner\ifhmode%
  \def\at@end@of@kframe{\end{minipage}}%
  \begin{minipage}{\columnwidth}%
 \fi\fi%
 \def\FrameCommand##1{\hskip\@totalleftmargin \hskip-\fboxsep
 \colorbox{shadecolor}{##1}\hskip-\fboxsep
     % There is no \\@totalrightmargin, so:
     \hskip-\linewidth \hskip-\@totalleftmargin \hskip\columnwidth}%
 \MakeFramed {\advance\hsize-\width
   \@totalleftmargin\z@ \linewidth\hsize
   \@setminipage}}%
 {\par\unskip\endMakeFramed%
 \at@end@of@kframe}
\makeatother

\definecolor{shadecolor}{rgb}{.97, .97, .97}
\definecolor{messagecolor}{rgb}{0, 0, 0}
\definecolor{warningcolor}{rgb}{1, 0, 1}
\definecolor{errorcolor}{rgb}{1, 0, 0}
\newenvironment{knitrout}{}{} % an empty environment to be redefined in TeX

\usepackage{alltt}
\usepackage{geometry}
\geometry{verbose,tmargin=1in,bmargin=1in,lmargin=1in,rmargin=1in}
%\usepackage{fancyhdr}
%\pagestyle{fancy}
\setlength{\parskip}{\smallskipamount}
\setlength{\parindent}{0pt}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{float}
\usepackage[authoryear]{natbib}
\DeclareMathOperator*{\argmax}{arg\,max}
\usepackage{graphicx}
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\begin{document}


\title{Predicting Voxel Responses using fMRI Data\\
Stat 215A, Fall 2014}


\author{Stefanus Candra\\
candrastefan@berkeley.edu}

\maketitle



\section{Introduction}
Hubel and Wiesel won a Nobel prize in 1981 for their discoveries concerning information processing in the visual system. They found that neuron activities in the visual cortex are specific to certain features, such as angle, shape, and patterns. These features can be represented by Gabor wavelets. When a person is shown an image, specific regions, called voxels, in the visual cortex become active and register readings that can be picked up by fMRI machine. The aim of this project is to find a model that learns the relationship between Gabor wavelets of different images and voxels responses with hope that the model can predict or reconstruct images based on fMRI readings of a person's brain activities.

\section{Data}

The two most important data set are \texttt{fit\_feat} and \texttt{resp\_dat}. \texttt{fit\_feat} contains Gabor wavelets of 1750 transformed images used as features of training set, while \texttt{resp\_dat} contains continuous real valued responses from the 20 voxels in the brain to the 1750 images which are used as responses of training set. The transformed images in \texttt{fit\_feat} are obtained from transforming intensity values in grayscale images using complex Gabor wavelet pyramid. Each transformed image in \texttt{fit\_feat} has 10921 continous real valued features which are the real parts of Gabor wavelets of the image. The data set also contains \texttt{val\_feat} which is a separate set of 120 transformed images. The end goal of this project is to find a suitable model and predict the responses to images in \texttt{val\_feat}. The data set seems to have been cleaned. There are no missing or corrupt values and boxplots on the responses in \texttt{resp\_dat} suggest that there are no outliers.

\section{Exploration}

We would like to know the distribution, correlation, and range of values of the features and responses to get a better understanding of the data. However, the features are high dimensional at 10921, therefore data exploration that can be done on the features with reasonable ease is limited. Summary statistics of \texttt{fit\_feat} shows that the range of the feature values is relatively small at 0 to 3.82 with mean 0.092 and median 0.037. Because of their small values and range, the feature values are not standardized. Not standardizing the features in this case favor interpretability at, hopefully, a small expense to performances of models which will be developed in later sections.\\
\\
On the other hand, the responses are in relatively low dimension at 20. Figure \ref{all.response.boxplot.qqplot} shows that the voxel responses are normally distributed at mean 0. This is good because that means it is not necessary to standardize the responses. Figure \ref{all.response.corr} shows that some voxel responses are highly correlated, while others are not.

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.4]{{voxel.response.boxplot}.png}
\includegraphics[scale = 0.4]{{voxel.response.qqplot}.png}
\caption{Top: boxplot of voxel responses, bottom: Q-Q plot of voxel responses to theoretical normal distribution. The boxplot shows that the voxel responses have approximately mean 0 and the Q-Q plot shows that the responses are close to being normally distributed.}
\label{all.response.boxplot.qqplot}
\end{figure}

\begin{figure}[h!]
\centering
\includegraphics[scale = 0.4]{{voxel.response.corr}.png}
\caption{Correlation plot between voxel responses. Voxel 1 to 9 are heavily correlated with each other, while voxel 10, 11, 13, 16, and 20 are almost not correlated with any other voxel.}
\label{all.response.corr}
\end{figure}

\section{Model Selection}

This is a high dimensional linear regression problem because the responses have continuous values and the number of features is large. Some choices of models for high dimensional linear regression problem are LASSO, ridge, PCR (Principal Component Regression), and PLS (Partial Least Square). LASSO and ridge are ordinary least-squares (OLS) regression with penalization on the coefficient/beta estimates. LASSO and ridge add L1 and L2-norm of the coefficient estimates respectively to the loss function of the OLS regression. On the other hand, PCR and PLS work by dimensionality reduction by finding principal components. PCR works by using PCA(Principal Component Analysis) to find orthogonal linear combinations of raw features that cover the highest variances to define new features and then use the new features in OLS. PLS is a variation of PCR that does SVD(Singular Value Decomposition) on $X^TY$ instead of $X^TX$ in PCR where $X$ is the design matrix and $Y$ is the response matrix.\\
\\
In subsequent sections of this report, only performances LASSO and ridge are analysed. This is because on preliminary analysis, running PCR and PLS on the training data using \texttt{R}'s \texttt{pls} package takes too long while seemingly producing comparable performance with LASSO and ridge in terms of residual sum of squares (RSS). On top of that, the PCR and PLS models consume a lot of disk space.

% Due to the large size of the data set, it is difficult to do cross-validation.
% As such, I suggest partitioning your data into training, validation, and testing
% sets (see 7.2 in Hastie for advice). You should use the training and validation
% sets to select your model and check performance with the validation set. The sizes
% of the subsets and how you want to partition the data are up to you.

\subsection{Model Performances}
% For LASSO, please compare the following model selection criteria: 
% CV, ES-CV, AIC, AICc, and BIC, for selecting the smoothing parameter in LASSO.
% Describe the strengths and weaknesses of the different criteria.
LASSO and ridge have solution paths corresponding to the lambda or penalization/shrinkage parameter and we want to choose the best lambda for each model. The metrics used to select the best model are RSS (Residual Sum of Squares), ES (Estimation Stability), AIC (Akaike Information Criterion), BIC (Bayesian Information Criterion), AICC (Akaike Information Criterion Corrected) from 10-fold cross validation. RSS is the standard and is intuitive; it gives empirical performance in generalization error. However, when applied to the sparse modeling method like LASSO, cross validation RSS leads to models that are unstable in high-dimensions, and consequently not suited for reliable interpretation (Lim, Yu 2013). ES attempts to correct that problem by calculating the variation in the responses across cross validation folds. AIC and BIC give theoretical measure of performance by calculating the log likelihood of the data penalized by the degree of freedoms or number of parameters. AIC and BIC struggle to find the right balance in how much to penalize the number of parameters. Lastly, AICC is simply AIC with finite sample size correction.\\
\\
For faster computation and to make the results from LASSO and ridge models comparable, we fix the sequence of lambdas to 20 values between [0.3, 0.003] inclusive instead of letting \texttt{R}'s \texttt{glmnet} package. We use this range of lambda because preliminary analysis shows that small RSS for both models are obtained when lambda is in this range.\\
\\
Because of 10-fold cross validation, the fact that LASSO and ridge have solution paths, and the fact that there are 20 voxel responses, each metric has values in 3 dimensions, 10 x 20 x 20, corresponding to the number of folds, number of voxels, and number of lambdas respectively. We want to boil each metric down to 1 x 1 x 20, i.e. a number for each choice of lambda. To do that, for each metric, we take the mean over the 10 folds of cross validation and over the 20 voxel responses. So, each column of metric in Table \ref{lasso.table} and \ref{ridge.table} represents the mean over 10 cross validation folds and over 20 voxel responses.\\
\\

% latex table generated in R 3.1.2 by xtable 1.7-4 package
% Thu Dec 11 13:57:02 2014
\begin{table}[ht]
\centering
\begin{tabular}{ccccccc}
  \hline
lambda & RSS & ES & VarY & AIC & BIC & AICC \\ 
  \hline
0.30 & 174.67 & 1.00e+28 & 0.01 & -3474.11 & -3474.11 & -3474.11 \\ 
  0.28 & 174.67 & 1.00e+28 & 0.01 & -3474.11 & -3474.11 & -3474.11 \\ 
  0.27 & 174.65 & 3.58e+02 & 0.01 & -3473.09 & -3469.88 & -3473.09 \\ 
  0.25 & 174.58 & 6.19e+01 & 0.02 & -3472.71 & -3466.82 & -3472.71 \\ 
  0.24 & 174.49 & 2.67e+01 & 0.04 & -3473.01 & -3465.50 & -3473.00 \\ 
  0.22 & 174.29 & 1.69e+01 & 0.07 & -3472.37 & -3458.43 & -3472.36 \\ 
  0.21 & 173.99 & 1.32e+01 & 0.12 & -3473.46 & -3455.23 & -3473.44 \\ 
  0.19 & 173.01 & 1.18e+01 & 0.23 & -3478.72 & -3450.30 & -3478.67 \\ 
  0.17 & 171.71 & 1.09e+01 & 0.43 & -3489.69 & -3458.59 & -3489.64 \\ 
  0.16 & 170.34 & 1.03e+01 & 0.75 & -3498.74 & -3457.45 & -3498.65 \\ 
  0.14 & 168.81 & 9.81e+00 & 1.20 & -3507.65 & -3451.88 & -3507.49 \\ 
  0.13 & 167.13 & 9.45e+00 & 1.83 & -3517.58 & -3445.73 & -3517.33 \\ 
  0.11 & 165.43 & 9.25e+00 & 2.68 & -3526.13 & -3433.37 & -3525.72 \\ 
  0.10 & 163.32 & 9.17e+00 & 3.81 & -3533.93 & -3406.85 & -3533.17 \\ 
  0.08 & 160.24 & 9.33e+00 & 5.39 & -3542.82 & -3356.75 & -3541.20 \\ 
  0.07 & 156.74 & 9.47e+00 & 7.71 & -3554.00 & -3301.98 & -3551.03 \\ 
  0.05 & 153.42 & 9.43e+00 & 11.01 & -3525.72 & -3103.73 & -3517.31 \\ 
  0.03 & 150.63 & 9.29e+00 & 15.83 & -3411.75 & -2602.62 & -3379.47 \\ 
  0.02 & 150.02 & 9.35e+00 & 24.92 & -2933.97 & -824.01 & -2670.69 \\ 
  0.00 & 198.87 & 8.80e+00 & 86.41 & 2180.24 & 16808.34 & -10722.82 \\ 
   \hline
\end{tabular}
\caption{LASSO regression performance metrics from 10-fold cross validation for each value of shrinkage parameter, lambda. The performance metrics represent the mean values over all voxels.} 
\label{lasso.table}
\end{table}


From Table \ref{lasso.table}, the best lambda for LASSO seems to be 0.03 because it has low RSS, the "dip"
of ES, as well as low AIC, BIC, and AICC.\\
\\

% latex table generated in R 3.1.2 by xtable 1.7-4 package
% Thu Dec 11 13:57:02 2014
\begin{table}[ht]
\centering
\begin{tabular}{ccccccc}
  \hline
lambda & RSS & ES & VarY & AIC & BIC & AICC \\ 
  \hline
0.30 & 147.52 & 9.25 & 24.81 & 17067.86 & 72881.03 & -7461.33 \\ 
  0.28 & 147.61 & 9.22 & 25.49 & 17068.71 & 72881.88 & -7460.48 \\ 
  0.27 & 147.70 & 9.23 & 25.98 & 17069.53 & 72882.70 & -7459.66 \\ 
  0.25 & 147.81 & 9.23 & 26.37 & 17070.63 & 72883.80 & -7458.56 \\ 
  0.24 & 147.94 & 9.22 & 26.81 & 17071.99 & 72885.16 & -7457.19 \\ 
  0.22 & 148.08 & 9.20 & 27.38 & 17073.48 & 72886.65 & -7455.71 \\ 
  0.21 & 148.27 & 9.18 & 28.06 & 17075.35 & 72888.52 & -7453.84 \\ 
  0.19 & 148.51 & 9.18 & 28.84 & 17077.76 & 72890.93 & -7451.43 \\ 
  0.17 & 148.80 & 9.17 & 29.65 & 17080.82 & 72893.99 & -7448.36 \\ 
  0.16 & 149.17 & 9.16 & 30.51 & 17084.62 & 72897.79 & -7444.57 \\ 
  0.14 & 149.62 & 9.15 & 31.48 & 17089.38 & 72902.55 & -7439.81 \\ 
  0.13 & 150.20 & 9.13 & 32.63 & 17095.35 & 72908.52 & -7433.83 \\ 
  0.11 & 150.94 & 9.11 & 34.02 & 17103.05 & 72916.22 & -7426.14 \\ 
  0.10 & 151.92 & 9.10 & 35.73 & 17113.22 & 72926.38 & -7415.97 \\ 
  0.08 & 153.27 & 9.08 & 37.85 & 17127.06 & 72940.23 & -7402.13 \\ 
  0.07 & 155.18 & 9.06 & 40.63 & 17146.57 & 72959.74 & -7382.62 \\ 
  0.05 & 158.08 & 9.03 & 44.51 & 17175.87 & 72989.04 & -7353.31 \\ 
  0.03 & 163.02 & 9.00 & 50.58 & 17224.50 & 73037.67 & -7304.69 \\ 
  0.02 & 173.47 & 8.97 & 62.44 & 17322.73 & 73135.90 & -7206.46 \\ 
  0.00 & 220.46 & 8.81 & 110.96 & 17700.76 & 73513.93 & -6828.43 \\ 
   \hline
\end{tabular}
\caption{Ridge regression performance metrics from 10-fold cross validation for each value of shrinkage parameter, lambda. The performance metrics represent the mean values over all voxels.} 
\label{ridge.table}
\end{table}


From Table \ref{ridge.table}, the best lambda for ridge seems to be 0.3. The metrics are improving with increasing lambda, so it could be the case that the best lambda is higher than 0.3. However, computing the model for large number of lambdas take a lot of time and consume a lot of disk space. Hence, we have to strike a balance in theoretical and practical model performances. From this section onward, we will use LASSO with lambda = 0.03 and ridge with lambda = 0.3.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.7]{{lasso.ridge.rss.plot}.png}
\caption{Plot of 10-fold cross-validation mean RSS of all voxel responses over lambda}
\label{lasso.ridge.rss.plot}
\end{figure}

\subsection{Correlation}
The performance indicator used in the Gallant lab is the correlation between the fitted values and observed values on a separate validation set. In this project, we use 70\% of the original training data \texttt{fit\_feat} and \texttt{resp\_dat} as the training set and the remaining 30\% as the validation set. Since we are not using cross validation, it is important to make sure that the distribution of responses in both the training and validation sets are similar. If the training and validation sets are not comparable, the trained models will perform badly on the validation set. Figure \ref{training.validation.boxplot.qqplot} shows that the responses of the training and validation set are approximately normally distributed with mean 0.

\begin{figure}[h!]
\centering
\includegraphics[width=0.7\textwidth]{{training.response.boxplot}.png}
\includegraphics[width=0.7\textwidth]{{validation.response.boxplot}.png}
\begin{subfigure}{.49\textwidth}
  \includegraphics[width=\textwidth]{{training.response.qqplot}.png}
\end{subfigure}
\begin{subfigure}{.49\textwidth}
  \includegraphics[width=\textwidth]{{validation.response.qqplot}.png}
\end{subfigure}
\caption{The boxplots show that the training and validation sets have mean 0, while the Q-Q plots show that they are close to being normally distributed.}
\label{training.validation.boxplot.qqplot}
\end{figure}

Table \ref{cor.table} shows the correlation between fitted values and observed values in the validation set for all 20 voxels. LASSO with lambda = 0.03 have higher correlation than ridge with lambda = 0.3 for most voxels and is therefore the better model according to Pearson correlation metric.

% latex table generated in R 3.1.2 by xtable 1.7-4 package
% Thu Dec 11 13:57:02 2014
\begin{table}[ht]
\centering
\begin{tabular}{rrrrrrrrrrr}
  \hline
 & voxel 1 & voxel 2 & voxel 3 & voxel 4 & voxel 5 & voxel 6 & voxel 7 & voxel 8 & voxel 9 & voxel 10 \\ 
  \hline
LASSO & 0.46 & 0.56 & 0.49 & 0.50 & 0.47 & 0.50 & 0.52 & 0.52 & 0.57 & 0.29 \\ 
  Ridge & 0.47 & 0.54 & 0.47 & 0.51 & 0.47 & 0.47 & 0.46 & 0.49 & 0.50 & 0.33 \\ 
   \hline
\end{tabular}
\end{table}
% latex table generated in R 3.1.2 by xtable 1.7-4 package
% Thu Dec 11 13:57:02 2014
\begin{table}[ht]
\centering
\begin{tabular}{rrrrrrrrrrr}
  \hline
 & voxel 11 & voxel 12 & voxel 13 & voxel 14 & voxel 15 & voxel 16 & voxel 17 & voxel 18 & voxel 19 & voxel 20 \\ 
  \hline
LASSO & 0.32 & 0.41 & 0.33 & 0.33 & 0.49 & 0.27 & 0.33 & 0.54 & 0.35 & 0.20 \\ 
  Ridge & 0.36 & 0.41 & 0.33 & 0.33 & 0.47 & 0.33 & 0.34 & 0.50 & 0.34 & 0.30 \\ 
   \hline
\end{tabular}
\caption{Pearson correlation between observed voxel responses in the validation set and predicted voxel responses.} 
\label{cor.table}
\end{table}


\section{Model Diagnostics}
% Choose a couple of your models to further investigate (you may restrict your
% further analysis to one or two voxels). Check the fit of your models. Are there
% any outliers? Discuss the stability of your prediction results and of the models?

We can see how well the models fit the validation set by looking at the residuals as well. Figure \ref{lasso.ridge.res.plot} shows the residuals of voxel 1 responses with LASSO (lambda = 0.03) and ridge (lambda = 0.3). The plot shows that the residuals are quite random around 0, so the fits are good in that we do not need to add more terms to the models.

\begin{figure}[h!]
\centering
\includegraphics[scale=0.6]{{lasso.ridge.res.v1.plot}.png}
\caption{Residuals of voxel 1 responses in validation set}
\label{lasso.ridge.res.plot}
\end{figure}

\subsection{Stability of Prediction Results and Model}
\label{pred.res.stability}




We are also interested in the stability of prediction results to ensure robustness of the models. One way to measure it is by building models with different training sets, use them to predict the same testing/validation set, and calculate the variation in predictions. In this case, the testing/validation set used is the same 30\% of the original training data as described in the previous section. The training sets are obtained by using different 90\% of the training set described in the previous section, which is 70\% of the original training data itself, in a manner similar to cross validation. So, we have 10 training sets each using 90\% * 70\% = 63\% of the original training data.\\
\\
We have 10 slightly different models from 10 different training sets as described above and we use them to predict on the validation set. We restrict this analysis to voxel 1. The number of observations in the validation set for voxel 1 is 525. After predicting, we have 10 lists of prediction from the 10 models, each having the same size as the validation set. So, for each observed response $r_i, i = 1$ to $525$, we have 10 different predictions $p_{i1}, ..., p_{i10}$. We can calculate the variance of these 10 predictions $p_{i1}, ..., p_{i10}$ and take its mean of the variances over all 525 responses to get a measure of stability of prediction results. The lower the value, the better it is because that means the mean variance of predictions is small. The mean variance of predictions of voxel 1 responses for LASSO is 0.0034916, while that for ridge is 0.0034636.\\
\\
We can get a measure of model stability by following the procedure in \ref{pred.res.stability} as well. Only now, instead of getting the mean variance of predictions, we calculate the mean variance of the coefficients of the 10 different models. The mean variance of coefficients of LASSO for voxel 1 is \ensuremath{1.2658824\times 10^{-5}} and that of ridge for voxel 1 is \ensuremath{3.0495231\times 10^{-6}}.

\section{Model Interpretation}
% Interpreting the models: Try to interpret your models. Do they share
% any features? Which predictors are important for which voxels?
% What features are stable across different bootstrap samples?
% Could you do hypothesis testing on the estimated parameters? How?
% What can be learned about how voxels respond to images?
% fit_stim.csv may be useful here.

In this scenario, both LASSO and ridge models are trained on the full \texttt{fit\_feat} and \texttt{resp\_dat} data. We will focus on the LASSO model since we consider it the better model according to various performance metrics given in previous section.\\
\\
Interestingly, LASSO has the same 138 features with non-zero coefficients for all voxel responses. However, the coefficients are different for each voxel. Also, these features only range from 1 to 1677, therefore the other 9000 features that come later are ignored. The coefficients of these 138 features found in the ridge model for all voxel responses are non-zero too as expected. So, LASSO and ridge share 138 features for all voxel responses.\\
\\
However, non-zero does not mean significance. To know which features are important or significant for each voxel, we have to do hypothesis testing. Lockhart, Tibshirani, and Taylor proposed a simple test statistic based on LASSO fitted values called the "covariance test statistic" (Lockhart, Tibshirani \url{http://statweb.stanford.edu/~tibs/ftp/covtest.pdf}). Alternatively, one can create many non-parametric bootstrap samples as artificial training sets and get the distribution of coefficients per feature per voxel. However, this takes a long time and is very expensive computationally because many samples, in the order of 1000 upwards, have to be generated to get decently good distributions of coefficients. As a toy example, we generated 10 non-parametric bootstrap samples from \texttt{fit\_feat} and \texttt{resp\_dat}, trained a LASSO model for each sample, and found that there were 20 features with non-zero coefficients that appeared in 10 out of 10 samples and 39 such features that appeared in 9 out of 10 samples.\\
\\
The above finding shows that there were at most 138 significant features with non-zero coefficients out of almost 11000 features. This hints that voxels only show strong responses at certain Gabor wavelets. This reinforces the findings by Nobel prize winner Hubel and Wiesel that neuron activities in the visual cortex are specific to certain features, such as angle, shape, and patterns. This suggests that the 20 voxels in the data set are only sensitive to at most 138 Gabor wavelets.

\end{document}

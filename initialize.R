# Script for initialization

# Load the necessary library
require(ggplot2)
require(plyr)
require(dplyr)
require(glmnet)
require(gridExtra)
require(reshape2)
require(xtable)
require(foreach)
require(doParallel)
require(pls)
require(glmnetcr)
require(doMC)
require(caret)

# Set the working directory
work.dir <- file.path("~", "Stat215a", "final_project")
setwd(work.dir)

source("utils.R")
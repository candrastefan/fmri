source("initialize.R")

load("lasso.best.fit.Rda")
load("ridge.best.fit.Rda")
load("lasso.bootstrap.Rda")
load("ridge.bootstrap.Rda")

# Features shared between LASSO and ridge
nvoxel <- 20
nonzero.beta <- lapply(1:nvoxel, function(x) which(lasso.best.fit$beta[[x]] != 0))
lapply(lasso.best.fit$beta, function(beta) beta[nonzero.beta[[1]]])
lapply(nonzero.beta, function(x) lasso.best.fit$beta[[1]])

# Features stable across different bootstrap samples
which(table(unlist(lapply(lasso.bootstrap, function(x) which(x != 0)))) == 10)


# Predictors important for which voxels
# For each voxel get the one with highest coefficient, maybe not so good
unlist(lapply(lasso.best.fit$beta, function(x) which(x == max(x))))
unlist(lapply(ridge.best.fit$beta, function(x) which(x == max(x))))

source("initialize.R")

# Predict on validation set. 
# Get the correlation between observed and fitted values for all 20 voxels.
load("lasso.fit.Rda")
load("ridge.fit.Rda")
load("training_validation.RData")

lasso.lambda.min <- 0.03426
ridge.lambda.min <- 0.300
save(lasso.lambda.min, ridge.lambda.min, file = "lambda.min.Rda")

lasso.pred <- predict(lasso.fit, validation.design, s = lasso.lambda.min)
lasso.pred <- lasso.pred[,,1]
ridge.pred <- predict(ridge.fit, validation.design, s = ridge.lambda.min)
ridge.pred <- ridge.pred[,,1]

lasso.cor <- diag(cor(lasso.pred, validation.response))
ridge.cor <- diag(cor(ridge.pred, validation.response))
cor.df <- data.frame(LASSO = lasso.cor, Ridge = ridge.cor)
cor.df <- t(cor.df)
colnames(cor.df) <- paste("voxel", 1:ncol(cor.df))
save(cor.df, file = "cor.df.Rda")

# Diagnostics

# Check the fit of LASSO and ridge
voxel <- 1
lasso.residual.v1 <- (validation.response - lasso.pred)[, voxel]
ridge.residual.v1 <- (validation.response - ridge.pred)[, voxel]

# Plot the LASSO and ridge residual
lasso.ridge.res.v1.plot <- ggplot() +
  geom_point(aes(x = lasso.pred[, voxel], y = lasso.residual.v1, color = "LASSO"), alpha = 0.5) +
  geom_point(aes(x = lasso.pred[, voxel], y = ridge.residual.v1, color = "Ridge"), alpha = 0.5) +
  xlab("Fitted Values") +
  ylab("Residual") +
  scale_color_discrete(name = "Classifier")
ggsave(lasso.ridge.res.v1.plot, file = "lasso.ridge.res.v1.plot.png")

# Stability of models and prediction results
# Train lasso and ridge using a subset of the data
# For each fold, get the coefficients at lasso.lambda.min and ridge.lambda.min
# Also, do prediction on the validation set

nfolds <- 10
test.sets.indices <- createFolds(1:nrow(training.design), k= nfolds, list = T, returnTrain = F)

training.xs <- lapply(test.sets.indices, function(indices) training.design[-indices, ])
training.ys <- lapply(test.sets.indices, function(indices) training.response[-indices, ])

lasso.stabilities <- cv.glmnet.stabilities(alpha = 1, lambda.min = lasso.lambda.min, 
                                           training.xs = training.xs,
                                           training.ys = training.ys,
                                           validation.design)

ridge.stabilities <- cv.glmnet.stabilities(alpha = 0, lambda.min = ridge.lambda.min, 
                                           training.xs = training.xs,
                                           training.ys = training.ys,
                                           validation.design)
save(lasso.stabilities, ridge.stabilities, file = "lasso.ridge.stabilities.Rda")

lasso.preds.v1 <- lapply(lasso.stabilities, function(x) x$prediction[,,1][, 1])
ridge.preds.v1 <- lapply(ridge.stabilities, function(x) x$prediction[,,1][, 1])

lasso.pred.v1.stability <- calculate.pred.stability(lasso.preds.v1)
ridge.pred.v1.stability <- calculate.pred.stability(ridge.preds.v1)
save(lasso.pred.v1.stability, ridge.pred.v1.stability, file = "lasso.ridge.pred.v1.stability.Rda")

lasso.betas.v1 <- lapply(lasso.stabilities, function(x) x$beta[[1]][, 1])
ridge.betas.v1 <- lapply(ridge.stabilities, function(x) x$beta[[1]][, 1])

lasso.beta.v1.stability <- calculate.pred.stability(lasso.betas.v1)
ridge.beta.v1.stability <- calculate.pred.stability(ridge.betas.v1)
save(lasso.beta.v1.stability, ridge.beta.v1.stability, file = "lasso.ridge.beta.v1.stability.Rda")